########## INSTALANDO AS FERRAMENTAS ##########
O primeiro passo é instalar as ferramentas, basta copiar os comandos abaixo para isso:

sudo apt install git
sudo apt-get install nodejs
sudo apt-get install npm
npm i react-scripts

Alem disso, é opcional que você instale o VSCode, o qual pode ser baixado no seguinte site: https://code.visualstudio.com/download

########## OS PRIMEIROS PASSOS ##########

Crie uma pasta em seu computador, através do comando mkdir "nome da pasta" e, em seguida, digite o comando git clone https://github.com/juniorsnts/facedev

Certifique-se que o conteudo foi clonado.

Feito isso, abra seu gitlab e faca login, criando um novo repositorio.

Para subir esse repositorio para o gitlab, basicamente o passo a passo é:

git remote add origin https://gitlab.com/SEUREPOSITORIO/SUAPAST.git
git branch -M main
git push -uf origin main

É provavel que após o primeiro comando, de o seguinte erro:
error: remote origin already exists.

Isso acontece pois o a origem do fetch e push está setado para o repositorio e pasta juniorsnts/facedev.

Voce pode checar isso atraves do comando git remote -v, o qual, provavelmente, irá retornar:

"origin https://github.com/juniorsnts/facedev (fetch)
origin  https://github.com/juniorsnts/facedev (push)"

Isso é resolvido com um simples comando:

git remote set-url origin https://gitlab.com/SEUDIRETORIO/SUAPASTA

Após isso, basta repetir o passo a passo do remote add origin e demais passos e seu conteudo ira para seu gitlab.

Pronto, agora podemos ir para o proximo passo que é montar o Dockerfile, porém, antes vamos visualizar a aplicação.

Para isso, basta entrarmos no terminal, ir ate a pasta onde esta instalada a aplicacao e rodar o seguinte comando:

npm start

Se tudo der certo, seu browser deve abrir uma janela "localhost:3000" O cabecalho será "FaceDev", seguido de dados de algumas pessoas, o endereco eletronico destas pessoas e suas respectivas postagens.

Realizar esse npm start é totalmente opcional, porem, é altamente recomendavel, pois voce estara visualizando o conteudo da aplicação, o que torna seu trabalho mais compreensivel, alem de se certificar que tudo funciona.

Dito isso, vamos construir o docker file.

########## CONSTRUINDO O DOCKERFILE ##########

Para construir o Dockerfile, vamos utilizar o Visual Studio Code.

Para isso, basta abrir o mesmo. Se voce estiver no terminal, basta digira o comando " code ."

Uma vez aberto, clique no canto superior esquerdo na opcao File, em seguida Open Folder e escolha a pasta onde esta a aplicação.

Em seguida, clique novamente na opcao file e new folder, nomeando como Dockerfile.

A sintaxe sugerida para elaboracao deste Dockerfile é a seguinte:

FROM node
WORKDIR /app
COPY package.json .
RUN npm install -g npm@8.11.0
COPY . . 
CMD ["npm", "start"]

Nesse ponto, eu preciso te explicar algo: Voce concorda comigo que 2+2=4, certo? Assim como, 3+1=4 2x2=4 1+1+1+1=4.

O que quero dizer com isso é que se existem muitas maneiras matematicas de se chegar a um mesmo resultado, sendo que na criacao do dockerfile nao é diferente.

Da maneira como tentei estruturar o dockerfile, considerando que ele trabalha em camadas, alguns comandos vao servir de cache para os demais, alem, de por exemplo, a versao do npm ser atualizada e leve, o que irá aumentar a velocidade do trabalho e diminuir o tamanho do container, mas existem diversas outras maneiras de ser construido esse mesmo container que tambem irão funcionar.

Dito isso, vamos testar.

Clique em File e em seguida em save.

Apos isso, clique na opcao terminal e new terminal, rodando o seguinte comando

docker build -t nomedoseucontainer .

Como pode perceber, a opcao -t permite a voce inserir o nome que preferir ao seu container, o que acaba facilitando na hora de manusea-lo, ja que o docker da um nome/numeracao ao container aleatoriamente, dificultando gravar o nome do mesmo.

Para verificar se funcionou, basta rodar o comando

docker images

Dando tudo certo, podemos partir para o proximo passo, mas antes, vamos salvar as alteracoes efetuadas no gitlab.

Para isso, rode os seguintes comandos:

git add .
git commit -m "enviando o dockerfile"
git push -u origin main


########## CRIANDO A PIPELINE ##########

A pipeline pode ser formulada atraves do visual studio ou diretamente do site do gitlab. Neste caso, vamos optar em formular diretamente no site.

Para isso, basta acessar sua conta do GITLAB e na coluna a esquerda ir ate a opcao CI/CD > Editor.

Assim como no docker, existem diversos meios de construir a sintaxe, sendo que a que iremos optar é a seguinte:

stages:          
  - build
docker-build:      
  image: docker
  stage: build
  variables:
    IMAGE_NAME: "vou_passar:latest"
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
   - docker build -t $CI_REGISTRY/testedevops/devops/$IMAGE_NAME .
   - docker push $CI_REGISTRY/testedevops/devops/$IMAGE_NAME
   
Perceba que nessa sintaxe, eu inseri o caminho do meu diretorio no gitlab, que no caso em questao é "/testedevops/devops/".  Voce pode verificar o seu endereco no seguinte diretorio do Gitlab Package & Registries > Container Registery. 

Em seguida, vai encontrar na parte de baixo da tela alguns comandos, com a sintaxe "docker build -t registry.gitlab.com/SEUREPOSITORIO/SUAPASTA". Basta copiar e trocar.
   
Apos inserirmos essa sintaxe, basta clicar na opcao commit changes e checar se a pipeline foi bem sucedida.

Apos esse passo, nosso container vai estar registrado no repositorio do GITLAB, restando apenas um passo, que é inserir o mesmo junto ao Docker Hub.

########## DOCKER HUB ##########


Antes de subirmos a imagem do container para o dockerhub, é necessario criarmos uma conta no docker hub, acessa-la, ir no menu account settings > security e gerar um token.

O token é necessario para logarmos na conta do docker atraves da linha de comando do gitlab, pois sem ele, nossa senha ficaria totalmente exposta, gerando uma grave falha de seguranca.

Apos gerar o token, devemos salvar o mesmo como uma variavel no gitlab, pois dessa maneira, conseguiremos "chamar" o token sem expor o mesmo.

Para isso, volte ate o gitlab > settings > CI/CD > variables e salve o token como uma variavel.

Eu escolhi o nome da variavel como sendo " DOCKER_HUB_TOKEN ".

Feito isso, ja vamos conseguimos subir nossa imagem para o docker hub. Para isso, volte ate o menu CI/CD > Editor.

Para subir sua imagem, basta colocar a seguinte sintaxe:

stages:          
  - build
docker-build:      
  image: docker
  stage: build
  variables:
    IMAGE_NAME: "vou_passar:latest"
  services:
    - docker:dind
  before_script:
    # - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
   # - docker build -t $CI_REGISTRY/testedevops/devops/$IMAGE_NAME .
   # - docker push $CI_REGISTRY/testedevops/devops/$IMAGE_NAME
   - docker build -t luiz920/$IMAGE_NAME .
   - docker login -u luiz920 -p $DOCKER_HUB_TOKEN 
   - docker push luiz920/$IMAGE_NAME

Perceba que nesta sintaxe, o nome do meu diretorio no no docker hub eh "luiz920", por isso, altere tal informacao.

Ao inserirmos a "#" o comando é ignorado pela pipeline, constando apenas como comentario. 

Feito isso, sua imagem estara no repositorio do gitlab e do docker hub, finalizando assim nosso trabalho. Agora, basta correr para o abraco!
