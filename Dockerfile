FROM node
WORKDIR /app
COPY package.json .
RUN npm install -g npm@8.11.0
COPY . .
CMD ["npm", "start"]

